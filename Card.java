//package cardpaper;
/*
        The output line(s) has the following format:
        First line indicates the device and the name of the subject for easy recognition
        All other lines contain data:
        HomingTime,Angle,DistanceToTarget,TargetSize,PositioningTime,NumberOfErrors
        
        Also, first 20 trials are considered practice, so do not record results
*/
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.KeyEvent;
import java.util.Random;

public class Card extends JFrame
    implements MouseListener, MouseMotionListener, KeyListener, ActionListener{
  public static int CARD_WIDTH = 1016;
  public static int CARD_CENTER = CARD_WIDTH -378;
  public static int CARD_HEIGHT = 740;
  public static Color HOME_COLOR = Color.RED;
  public static Color TARGET_COLOR = Color.BLACK;
  public CardJPanel cardJPanel = new CardJPanel();
  public Button okButton=new Button(" OK ");
  public Dialog d=new Dialog(this,"Results of last trial session",true);
  public Dialog s=new Dialog(this,"Initialization",true);
  public Dialog e=new Dialog(this,"Experiment Ended",true);
  Label label1=new Label("Round of trials completed");
  Label label2=new Label("Average positioning time: ");
  Label label3=new Label("Average errors:");
  Label timeLabel;
  Label errorLabel;
  CloseFrame cF = new CloseFrame();
  Button GetUserAndDeviceInfoButton = new Button("Submit");
  public TextField deviceName = new TextField(20);
  public TextField subjectName = new TextField(20);
  public long start_time;
  public long homingTime;
  public boolean clock_running = false;
  public int x = 0;
  public int y = 0;
  public int target_x = 0;
  public int target_y = 0;
  public int miss_clicks = 0;
  int noTrials=0;
  double averageTime=0;
  double averageErrors=0;
  int targetSize=-1;
  int distanceToTarget=0;
  boolean trialArray[]=new boolean[200];
  int RandomNumber=-1;
  boolean FirstMovement=false;  
  int angle=0;

  public Card() {
    //initialize boolean array
    for(int i=0;i<200;i++)
	trialArray[i]=false;
    //generate first random trial
    GenerateNextTrial();
    //use that random number to set the next position and size
    SetNewPositionAndSize(RandomNumber);

    cardJPanel.addMouseMotionListener(this);
    cardJPanel.addMouseListener(this);
    cardJPanel.setLayout(new BorderLayout());
    addKeyListener(this);
    setContentPane(cardJPanel);
    setTitle("Card experiment 2");
    setSize(CARD_WIDTH+9, CARD_HEIGHT+28);
    setResizable(false);
    addWindowListener(cF);
    setVisible(true);
    s.setSize(200,200);
    s.setLayout(new FlowLayout(FlowLayout.CENTER));
    s.add(new Label("Name of device: "));
    s.add(deviceName);
    s.add(new Label("Name of test subject: "));
    s.add(subjectName);
    GetUserAndDeviceInfoButton.addActionListener(this);
    s.add(GetUserAndDeviceInfoButton);
    s.setVisible(true);
  }
  
  public class CloseFrame extends WindowAdapter {
          public void windowClosing(WindowEvent e)
          {
                  if(e.getSource()==d)
		  {
                        d.setVisible(false);
			d.remove(label1);
			d.remove(label2);
			d.remove(timeLabel);
			d.remove(label3);
			d.remove(errorLabel);
			d.dispose();
		  }
                  else 
                        System.exit(0);
          }
  }

  public static void main(String arg[]) {
    Card c = new Card();
  }

  public void mousePressed(MouseEvent mouseEvent) {
    clockStop();
  }

  public void mouseMoved(MouseEvent mouseEvent) {
    x = mouseEvent.getX();
    y = mouseEvent.getY();
    
    if(FirstMovement)
    {
        long totalHomingTime = System.currentTimeMillis() - homingTime;
        if(noTrials>20)
	        System.out.print(totalHomingTime + ",");
	FirstMovement=false;
    }
    if (clock_running) {
      HOME_COLOR = Color.GREEN;
    }
    else 
    {
      //handle the positioning box (on the bottom of the screen)
      //the positioning box size is 20x20
      if ( (x > CARD_CENTER - 10) && (x < CARD_CENTER + 10) &&
           (y > CARD_HEIGHT - 20) && (y < CARD_HEIGHT)
         ) 
      {
          HOME_COLOR = Color.YELLOW;
      }
      else //cursor outside the positioning box
      {
        HOME_COLOR = Color.RED;
      }
    }//end of else
    
    cardJPanel.repaint();
  }//end of mouseMoved

  public void keyTyped(KeyEvent keyEvent) {
    if (keyEvent.getKeyChar() == ' ') {
      if (HOME_COLOR == Color.YELLOW) {
        FirstMovement=true;
        clockStart();
      }
    }
  }

  public void clockStart() {
    /*target_x+=100;
    target_y+=100;
    if (target_x > 500) {
      target_x=50;
    }
    if (target_y > 500) {
      target_y=50;
    }*/
    miss_clicks = 0;
    clock_running = true;
    start_time = System.currentTimeMillis();
    homingTime = System.currentTimeMillis();
    //System.out.print("(" + x + "," + y + ")");
    TARGET_COLOR = Color.WHITE;
    HOME_COLOR = Color.GREEN;
    cardJPanel.repaint();
    noTrials++;
  }

  public void clockStop() {
          
    if (HOME_COLOR == Color.GREEN) {
      if ( 
          ( (targetSize==0) && (x > target_x) && x < (target_x+21) && (y > target_y) && y < (target_y+22) )||
          ( (targetSize==1) && (x > target_x) && x < (target_x+38) && (y > target_y) && y < (target_y+22) )||
          ( (targetSize==2) && (x > target_x) && x < (target_x+74) && (y > target_y) && y < (target_y+22) )||
          ( (targetSize==3) && (x > target_x) && x < (target_x+180) && (y > target_y) && y < (target_y+22) )
      ) 
      {
        clock_running = false;
        long total_time = System.currentTimeMillis() - start_time;
        averageTime += total_time;
        averageErrors += miss_clicks;
	//First 20 trials are considered practive, do not save data from them
        if(noTrials>20)
	{
		System.out.print(angle + ",");
                System.out.print(distanceToTarget + ",");
		System.out.print(targetSize);
	        System.out.print("," + total_time);
	        //System.out.print("\t(" + x + "," + y + ")");
	        System.out.println("," + miss_clicks);
	}
        HOME_COLOR = Color.RED;
        TARGET_COLOR = Color.BLACK;
        cardJPanel.repaint();
        //display results after each 20 trials
        if(noTrials%20==0)
        {
                averageErrors /=20;
                averageTime /=20;
                ResultsDialog(averageTime,averageErrors);
                averageTime=0;
                averageErrors=0;
                miss_clicks=0; 
        }
        //check if the experiment should end (first 20 trials are practice + 200 data collected)
        if(noTrials>=220)
        {
                //end of experiment trial for the device
		System.out.print("Experiment Complete");
                e.setSize(200,150);
                e.setLayout(new FlowLayout(FlowLayout.CENTER));
                e.add(new Label("Experiment Ended"));
                e.addWindowListener(cF);
                e.setVisible(true);
        }

    	//generate next random trial
    	//for now set it manually
    	GenerateNextTrial();
    	//use that random number to set the next position and size
    	SetNewPositionAndSize(RandomNumber);
      }//end of if within the position range
      else {
        miss_clicks++;
      }//end of else
    }//end of if HOME color Green
  }//end of void ClockStop()

  public void GenerateNextTrial()
  {
	//generate random number between 0 and 199, then check the trialArray, if false, set it to true, return 
        //generated numeber, else generate next random number. Do this until all the numbers in array are true.
	Random generator = new Random();	//uses time as seed by default
	RandomNumber = generator.nextInt(200);
    if(noTrials>20)
    {
      if(trialArray[RandomNumber]==false)
	{
		trialArray[RandomNumber]=true;
	}
      else
      {          
	while(trialArray[RandomNumber]==true)
        {
		RandomNumber = generator.nextInt(200);
                if(trialArray[RandomNumber]==false)
	        {       
		        trialArray[RandomNumber]=true;
			break;
	        }
                //failsafe, should never be reached
                if(noTrials>=220)
                        break;
	}//end of while
      }//end of else 
    }//end of if
  }//end of GenerateNextTrial()

  public void SetNewPositionAndSize(int i)
  {
	int xP=CARD_CENTER;
	int yP=CARD_HEIGHT-20;
	//there are better ways of doing this, but due to lack of time, I decided to hard code all the positions
        switch(i)
	{
		//Distance to the object is measured from the center top part of the positioning box
		//to the center of the object
                //1 cm is equal to distance of 35 pixels on the computers we tested on
		//The centers for different sizes are as follows:
		//Size 1 cm: center: x-10, y-11
		//Size 2 cm: center: x-18, y-11
		//Size 3 cm: center: x-37, y-11
		//Size 4 cm: center: x-90, y-11
		//The above need to be taken into account when calculating the positions
		//Ten angles are used, by dividing 120 degrees into 10 parts equally (i.e. 12,24,etc.)
		//When the angle is calculated, sins fnd cosines formulas were used to calculate length of x and y:
		//x = distance * cosine(angle)
		//y = distance * sin(angle)
		//and then results were translated into pixels by multiplying by 35, and rounding to nearest int

		//Cases 0-3 distance=1cm, angle=12, size varies, lengths: x=-34,y=-7
		case 0: target_x=xP-34-10;target_y=yP-7-11;targetSize=0;distanceToTarget=1;angle=12;break;
		case 1: target_x=xP-34-18;target_y=yP-7-11;targetSize=1;distanceToTarget=1;angle=12;break;
		case 2: target_x=xP-34-37;target_y=yP-7-11;targetSize=2;distanceToTarget=1;angle=12;break;
		case 3: target_x=xP-34-90;target_y=yP-7-11;targetSize=3;distanceToTarget=1;angle=12;break;
		//Cases 4-7 distance=2cm, angle=12, size varies, lengths: x=-68,y=-15
		case 4: target_x=xP-68-10;target_y=yP-15-11;targetSize=0;distanceToTarget=2;angle=12;break;
		case 5: target_x=xP-68-18;target_y=yP-15-11;targetSize=1;distanceToTarget=2;angle=12;break;
		case 6: target_x=xP-68-37;target_y=yP-15-11;targetSize=2;distanceToTarget=2;angle=12;break;
		case 7: target_x=xP-68-90;target_y=yP-15-11;targetSize=3;distanceToTarget=2;angle=12;break;
		//Cases 8-11 distance=4cm, angle=12, size varies, lengths: x=-137,y=-29
		case 8: target_x=xP-137-10;target_y=yP-29-11;targetSize=0;distanceToTarget=4;angle=12;break;
		case 9: target_x=xP-137-18;target_y=yP-29-11;targetSize=1;distanceToTarget=4;angle=12;break;
		case 10: target_x=xP-137-37;target_y=yP-29-11;targetSize=2;distanceToTarget=4;angle=12;break;
		case 11: target_x=xP-137-90;target_y=yP-29-11;targetSize=3;distanceToTarget=4;angle=12;break;
		//Cases 12-15 distance=8cm, angle=12, size varies, lengths: x=-274,y=-58
		case 12: target_x=xP-274-10;target_y=yP-58-11;targetSize=0;distanceToTarget=8;angle=12;break;
		case 13: target_x=xP-274-18;target_y=yP-58-11;targetSize=1;distanceToTarget=8;angle=12;break;
		case 14: target_x=xP-274-37;target_y=yP-58-11;targetSize=2;distanceToTarget=8;angle=12;break;
		case 15: target_x=xP-274-90;target_y=yP-58-11;targetSize=3;distanceToTarget=8;angle=12;break;
		//Cases 16-19 distance=16cm, angle=12, size varies, lengths: x=-547,y=-116
		case 16: target_x=xP-547-10;target_y=yP-116-11;targetSize=0;distanceToTarget=16;angle=12;break;
		case 17: target_x=xP-547-18;target_y=yP-116-11;targetSize=1;distanceToTarget=16;angle=12;break;
		case 18: target_x=xP-547-37;target_y=yP-116-11;targetSize=2;distanceToTarget=16;angle=12;break;
		case 19: target_x=xP-547-90;target_y=yP-116-11;targetSize=3;distanceToTarget=16;angle=12;break;

		//Cases 20-23 distance=1cm, angle=24, size varies, lengths: x=-32,y=-14
		case 20: target_x=xP-32-10;target_y=yP-14-11;targetSize=0;distanceToTarget=1;angle=24;break;
		case 21: target_x=xP-32-18;target_y=yP-14-11;targetSize=1;distanceToTarget=1;angle=24;break;
		case 22: target_x=xP-32-37;target_y=yP-14-11;targetSize=2;distanceToTarget=1;angle=24;break;
		case 23: target_x=xP-32-90;target_y=yP-14-11;targetSize=3;distanceToTarget=1;angle=24;break;
		//Cases 24-27 distance=2cm, angle=24, size varies, lengths: x=-64,y=-28
		case 24: target_x=xP-64-10;target_y=yP-28-11;targetSize=0;distanceToTarget=2;angle=24;break;
		case 25: target_x=xP-64-18;target_y=yP-28-11;targetSize=1;distanceToTarget=2;angle=24;break;
		case 26: target_x=xP-64-37;target_y=yP-28-11;targetSize=2;distanceToTarget=2;angle=24;break;
		case 27: target_x=xP-64-90;target_y=yP-28-11;targetSize=3;distanceToTarget=2;angle=24;break;
		//Cases 28-31 distance=4cm, angle=24, size varies, lengths: x=-128,y=-57
		case 28: target_x=xP-128-10;target_y=yP-57-11;targetSize=0;distanceToTarget=4;angle=24;break;
		case 29: target_x=xP-128-18;target_y=yP-57-11;targetSize=1;distanceToTarget=4;angle=24;break;
		case 30: target_x=xP-128-37;target_y=yP-57-11;targetSize=2;distanceToTarget=4;angle=24;break;
		case 31: target_x=xP-128-90;target_y=yP-57-11;targetSize=3;distanceToTarget=4;angle=24;break;
		//Cases 32-35 distance=8cm, angle=24, size varies, lengths: x=-256,y=-114
		case 32: target_x=xP-256-10;target_y=yP-114-11;targetSize=0;distanceToTarget=8;angle=24;break;
		case 33: target_x=xP-256-18;target_y=yP-114-11;targetSize=1;distanceToTarget=8;angle=24;break;
		case 34: target_x=xP-256-37;target_y=yP-114-11;targetSize=2;distanceToTarget=8;angle=24;break;
		case 35: target_x=xP-256-90;target_y=yP-114-11;targetSize=3;distanceToTarget=8;angle=24;break;
		//Cases 36-39 distance=16cm, angle=24, size varies, lengths: x=-512,y=-228
		case 36: target_x=xP-512-10;target_y=yP-228-11;targetSize=0;distanceToTarget=16;angle=24;break;
		case 37: target_x=xP-512-18;target_y=yP-228-11;targetSize=1;distanceToTarget=16;angle=24;break;
		case 38: target_x=xP-512-37;target_y=yP-228-11;targetSize=2;distanceToTarget=16;angle=24;break;
		case 39: target_x=xP-512-90;target_y=yP-228-11;targetSize=3;distanceToTarget=16;angle=24;break;
		
		//Cases 40-43 distance=1cm, angle=36, size varies, lengths: x=-28,y=-21
		case 40: target_x=xP-28-10;target_y=yP-21-11;targetSize=0;distanceToTarget=1;angle=36;break;
		case 41: target_x=xP-28-18;target_y=yP-21-11;targetSize=1;distanceToTarget=1;angle=36;break;
		case 42: target_x=xP-28-37;target_y=yP-21-11;targetSize=2;distanceToTarget=1;angle=36;break;
		case 43: target_x=xP-28-90;target_y=yP-21-11;targetSize=3;distanceToTarget=1;angle=36;break;
		//Cases 44-47 distance=2cm, angle=36, size varies, lengths: x=-57,y=-41
		case 44: target_x=xP-57-10;target_y=yP-41-11;targetSize=0;distanceToTarget=2;angle=36;break;
		case 45: target_x=xP-57-18;target_y=yP-41-11;targetSize=1;distanceToTarget=2;angle=36;break;
		case 46: target_x=xP-57-37;target_y=yP-41-11;targetSize=2;distanceToTarget=2;angle=36;break;
		case 47: target_x=xP-57-90;target_y=yP-41-11;targetSize=3;distanceToTarget=2;angle=36;break;
		//Cases 48-51 distance=4cm, angle=36, size varies, lengths: x=-113,y=-82
		case 48: target_x=xP-113-10;target_y=yP-82-11;targetSize=0;distanceToTarget=4;angle=36;break;
		case 49: target_x=xP-113-18;target_y=yP-82-11;targetSize=1;distanceToTarget=4;angle=36;break;
		case 50: target_x=xP-113-37;target_y=yP-82-11;targetSize=2;distanceToTarget=4;angle=36;break;
		case 51: target_x=xP-113-90;target_y=yP-82-11;targetSize=3;distanceToTarget=4;angle=36;break;
		//Cases 52-55 distance=8cm, angle=36, size varies, lengths: x=-227,y=-165
		case 52: target_x=xP-227-10;target_y=yP-165-11;targetSize=0;distanceToTarget=8;angle=36;break;
		case 53: target_x=xP-227-18;target_y=yP-165-11;targetSize=1;distanceToTarget=8;angle=36;break;
		case 54: target_x=xP-227-37;target_y=yP-165-11;targetSize=2;distanceToTarget=8;angle=36;break;
		case 55: target_x=xP-227-90;target_y=yP-165-11;targetSize=3;distanceToTarget=8;angle=36;break;
		//Cases 56-59 distance=16cm, angle=36, size varies, lengths: x=-453,y=-329
		case 56: target_x=xP-453-10;target_y=yP-329-11;targetSize=0;distanceToTarget=16;angle=36;break;
		case 57: target_x=xP-453-18;target_y=yP-329-11;targetSize=1;distanceToTarget=16;angle=36;break;
		case 58: target_x=xP-453-37;target_y=yP-329-11;targetSize=2;distanceToTarget=16;angle=36;break;
		case 59: target_x=xP-453-90;target_y=yP-329-11;targetSize=3;distanceToTarget=16;angle=36;break;

		//Cases 60-63 distance=1cm, angle=48, size varies, lengths: x=-23,y=-26
		case 60: target_x=xP-23-10;target_y=yP-26-11;targetSize=0;distanceToTarget=1;angle=48;break;
		case 61: target_x=xP-23-18;target_y=yP-26-11;targetSize=1;distanceToTarget=1;angle=48;break;
		case 62: target_x=xP-23-37;target_y=yP-26-11;targetSize=2;distanceToTarget=1;angle=48;break;
		case 63: target_x=xP-23-90;target_y=yP-26-11;targetSize=3;distanceToTarget=1;angle=48;break;
		//Cases 64-67 distance=2cm, angle=48, size varies, lengths: x=-46,y=-52
		case 64: target_x=xP-46-10;target_y=yP-52-11;targetSize=0;distanceToTarget=2;angle=48;break;
		case 65: target_x=xP-46-18;target_y=yP-52-11;targetSize=1;distanceToTarget=2;angle=48;break;
		case 66: target_x=xP-46-37;target_y=yP-52-11;targetSize=2;distanceToTarget=2;angle=48;break;
		case 67: target_x=xP-46-90;target_y=yP-52-11;targetSize=3;distanceToTarget=2;angle=48;break;
		//Cases 68-71 distance=4cm, angle=48, size varies, lengths: x=-94,y=-104
		case 68: target_x=xP-94-10;target_y=yP-104-11;targetSize=0;distanceToTarget=4;angle=48;break;
		case 69: target_x=xP-94-18;target_y=yP-104-11;targetSize=1;distanceToTarget=4;angle=48;break;
		case 70: target_x=xP-94-37;target_y=yP-104-11;targetSize=2;distanceToTarget=4;angle=48;break;
		case 71: target_x=xP-94-90;target_y=yP-104-11;targetSize=3;distanceToTarget=4;angle=48;break;
		//Cases 72-75 distance=8cm, angle=48, size varies, lengths: x=-187,y=-208
		case 72: target_x=xP-187-10;target_y=yP-208-11;targetSize=0;distanceToTarget=8;angle=48;break;
		case 73: target_x=xP-187-18;target_y=yP-208-11;targetSize=1;distanceToTarget=8;angle=48;break;
		case 74: target_x=xP-187-37;target_y=yP-208-11;targetSize=2;distanceToTarget=8;angle=48;break;
		case 75: target_x=xP-187-90;target_y=yP-208-11;targetSize=3;distanceToTarget=8;angle=48;break;
		//Cases 76-79 distance=16cm, angle=48, size varies, lengths: x=-374,y=-416
		case 76: target_x=xP-374-10;target_y=yP-416-11;targetSize=0;distanceToTarget=16;angle=48;break;
		case 77: target_x=xP-374-18;target_y=yP-416-11;targetSize=1;distanceToTarget=16;angle=48;break;
		case 78: target_x=xP-374-37;target_y=yP-416-11;targetSize=2;distanceToTarget=16;angle=48;break;
		case 79: target_x=xP-374-90;target_y=yP-416-11;targetSize=3;distanceToTarget=16;angle=48;break;

		//Cases 80-83 distance=1cm, angle=60, size varies, lengths: x=-18,y=-30
		case 80: target_x=xP-18-10;target_y=yP-30-11;targetSize=0;distanceToTarget=1;angle=60;break;
		case 81: target_x=xP-18-18;target_y=yP-30-11;targetSize=1;distanceToTarget=1;angle=60;break;
		case 82: target_x=xP-18-37;target_y=yP-30-11;targetSize=2;distanceToTarget=1;angle=60;break;
		case 83: target_x=xP-18-90;target_y=yP-30-11;targetSize=3;distanceToTarget=1;angle=60;break;
		//Cases 84-87 distance=2cm, angle=60, size varies, lengths: x=-35,y=-61
		case 84: target_x=xP-35-10;target_y=yP-61-11;targetSize=0;distanceToTarget=2;angle=60;break;
		case 85: target_x=xP-35-18;target_y=yP-61-11;targetSize=1;distanceToTarget=2;angle=60;break;
		case 86: target_x=xP-35-37;target_y=yP-61-11;targetSize=2;distanceToTarget=2;angle=60;break;
		case 87: target_x=xP-35-90;target_y=yP-61-11;targetSize=3;distanceToTarget=2;angle=60;break;
		//Cases 88-91 distance=4cm, angle=60, size varies, lengths: x=-70,y=-121
		case 88: target_x=xP-70-10;target_y=yP-121-11;targetSize=0;distanceToTarget=4;angle=60;break;
		case 89: target_x=xP-70-18;target_y=yP-121-11;targetSize=1;distanceToTarget=4;angle=60;break;
		case 90: target_x=xP-70-37;target_y=yP-121-11;targetSize=2;distanceToTarget=4;angle=60;break;
		case 91: target_x=xP-70-90;target_y=yP-121-11;targetSize=3;distanceToTarget=4;angle=60;break;
		//Cases 92-95 distance=8cm, angle=60, size varies, lengths: x=-140,y=-242
		case 92: target_x=xP-140-10;target_y=yP-242-11;targetSize=0;distanceToTarget=8;angle=60;break;
		case 93: target_x=xP-140-18;target_y=yP-242-11;targetSize=1;distanceToTarget=8;angle=60;break;
		case 94: target_x=xP-140-37;target_y=yP-242-11;targetSize=2;distanceToTarget=8;angle=60;break;
		case 95: target_x=xP-140-90;target_y=yP-242-11;targetSize=3;distanceToTarget=8;angle=60;break;
		//Cases 96-99 distance=16cm, angle=60, size varies, lengths: x=-280,y=-485
		case 96: target_x=xP-280-10;target_y=yP-485-11;targetSize=0;distanceToTarget=16;angle=60;break;
		case 97: target_x=xP-280-18;target_y=yP-485-11;targetSize=1;distanceToTarget=16;angle=60;break;
		case 98: target_x=xP-280-37;target_y=yP-485-11;targetSize=2;distanceToTarget=16;angle=60;break;
		case 99: target_x=xP-280-90;target_y=yP-485-11;targetSize=3;distanceToTarget=16;angle=60;break;

		//Cases 100-103 distance=1cm, angle=72, size varies, lengths: x=-11,y=-33
		case 100: target_x=xP-11-10;target_y=yP-33-11;targetSize=0;distanceToTarget=1;angle=72;break;
		case 101: target_x=xP-11-18;target_y=yP-33-11;targetSize=1;distanceToTarget=1;angle=72;break;
		case 102: target_x=xP-11-37;target_y=yP-33-11;targetSize=2;distanceToTarget=1;angle=72;break;
		case 103: target_x=xP-11-90;target_y=yP-33-11;targetSize=3;distanceToTarget=1;angle=72;break;
		//Cases 104-107 distance=2cm, angle=72, size varies, lengths: x=-22,y=-67
		case 104: target_x=xP-22-10;target_y=yP-67-11;targetSize=0;distanceToTarget=2;angle=72;break;
		case 105: target_x=xP-22-18;target_y=yP-67-11;targetSize=1;distanceToTarget=2;angle=72;break;
		case 106: target_x=xP-22-37;target_y=yP-67-11;targetSize=2;distanceToTarget=2;angle=72;break;
		case 107: target_x=xP-22-90;target_y=yP-67-11;targetSize=3;distanceToTarget=2;angle=72;break;
		//Cases 108-111 distance=4cm, angle=72, size varies, lengths: x=-43,y=-133
		case 108: target_x=xP-43-10;target_y=yP-133-11;targetSize=0;distanceToTarget=4;angle=72;break;
		case 109: target_x=xP-43-18;target_y=yP-133-11;targetSize=1;distanceToTarget=4;angle=72;break;
		case 110: target_x=xP-43-37;target_y=yP-133-11;targetSize=2;distanceToTarget=4;angle=72;break;
		case 111: target_x=xP-43-90;target_y=yP-133-11;targetSize=3;distanceToTarget=4;angle=72;break;
		//Cases 112-115 distance=8cm, angle=72, size varies, lengths: x=-87,y=-267
		case 112: target_x=xP-87-10;target_y=yP-267-11;targetSize=0;distanceToTarget=8;angle=72;break;
		case 113: target_x=xP-87-18;target_y=yP-267-11;targetSize=1;distanceToTarget=8;angle=72;break;
		case 114: target_x=xP-87-37;target_y=yP-267-11;targetSize=2;distanceToTarget=8;angle=72;break;
		case 115: target_x=xP-87-90;target_y=yP-267-11;targetSize=3;distanceToTarget=8;angle=72;break;
		//Cases 116-119 distance=16cm, angle=72, size varies, lengths: x=-173,y=-533
		case 116: target_x=xP-173-10;target_y=yP-533-11;targetSize=0;distanceToTarget=16;angle=72;break;
		case 117: target_x=xP-173-18;target_y=yP-533-11;targetSize=1;distanceToTarget=16;angle=72;break;
		case 118: target_x=xP-173-37;target_y=yP-533-11;targetSize=2;distanceToTarget=16;angle=72;break;
		case 119: target_x=xP-173-90;target_y=yP-533-11;targetSize=3;distanceToTarget=16;angle=72;break;

		//Cases 120-123 distance=1cm, angle=84, size varies, lengths: x=-4,y=-35
		case 120: target_x=xP-4-10;target_y=yP-35-11;targetSize=0;distanceToTarget=1;angle=84;break;
		case 121: target_x=xP-4-18;target_y=yP-35-11;targetSize=1;distanceToTarget=1;angle=84;break;
		case 122: target_x=xP-4-37;target_y=yP-35-11;targetSize=2;distanceToTarget=1;angle=84;break;
		case 123: target_x=xP-4-90;target_y=yP-35-11;targetSize=3;distanceToTarget=1;angle=84;break;
		//Cases 124-127 distance=2cm, angle=84, size varies, lengths: x=-7,y=-70
		case 124: target_x=xP-7-10;target_y=yP-70-11;targetSize=0;distanceToTarget=2;angle=84;break;
		case 125: target_x=xP-7-18;target_y=yP-70-11;targetSize=1;distanceToTarget=2;angle=84;break;
		case 126: target_x=xP-7-37;target_y=yP-70-11;targetSize=2;distanceToTarget=2;angle=84;break;
		case 127: target_x=xP-7-90;target_y=yP-70-11;targetSize=3;distanceToTarget=2;angle=84;break;
		//Cases 128-131 distance=4cm, angle=84, size varies, lengths: x=-15,y=-139
		case 128: target_x=xP-15-10;target_y=yP-139-11;targetSize=0;distanceToTarget=4;angle=84;break;
		case 129: target_x=xP-15-18;target_y=yP-139-11;targetSize=1;distanceToTarget=4;angle=84;break;
		case 130: target_x=xP-15-37;target_y=yP-139-11;targetSize=2;distanceToTarget=4;angle=84;break;
		case 131: target_x=xP-15-90;target_y=yP-139-11;targetSize=3;distanceToTarget=4;angle=84;break;
		//Cases 132-135 distance=8cm, angle=84, size varies, lengths: x=-29,y=-278
		case 132: target_x=xP-29-10;target_y=yP-278-11;targetSize=0;distanceToTarget=8;angle=84;break;
		case 133: target_x=xP-29-18;target_y=yP-278-11;targetSize=1;distanceToTarget=8;angle=84;break;
		case 134: target_x=xP-29-37;target_y=yP-278-11;targetSize=2;distanceToTarget=8;angle=84;break;
		case 135: target_x=xP-29-90;target_y=yP-278-11;targetSize=3;distanceToTarget=8;angle=84;break;
		//Cases 136-139 distance=16cm, angle=84, size varies, lengths: x=-59,y=-557
		case 136: target_x=xP-59-10;target_y=yP-557-11;targetSize=0;distanceToTarget=16;angle=84;break;
		case 137: target_x=xP-59-18;target_y=yP-557-11;targetSize=1;distanceToTarget=16;angle=84;break;
		case 138: target_x=xP-59-37;target_y=yP-557-11;targetSize=2;distanceToTarget=16;angle=84;break;
		case 139: target_x=xP-59-90;target_y=yP-557-11;targetSize=3;distanceToTarget=16;angle=84;break;

		//Cases 140-143 distance=1cm, angle=96, size varies, lengths: x=+4,y=-35
		case 140: target_x=xP+4-10;target_y=yP-35-11;targetSize=0;distanceToTarget=1;angle=96;break;
		case 141: target_x=xP+4-18;target_y=yP-35-11;targetSize=1;distanceToTarget=1;angle=96;break;
		case 142: target_x=xP+4-37;target_y=yP-35-11;targetSize=2;distanceToTarget=1;angle=96;break;
		case 143: target_x=xP+4-90;target_y=yP-35-11;targetSize=3;distanceToTarget=1;angle=96;break;
		//Cases 144-147 distance=2cm, angle=96, size varies, lengths: x=+7,y=-70
		case 144: target_x=xP+7-10;target_y=yP-70-11;targetSize=0;distanceToTarget=2;angle=96;break;
		case 145: target_x=xP+7-18;target_y=yP-70-11;targetSize=1;distanceToTarget=2;angle=96;break;
		case 146: target_x=xP+7-37;target_y=yP-70-11;targetSize=2;distanceToTarget=2;angle=96;break;
		case 147: target_x=xP+7-90;target_y=yP-70-11;targetSize=3;distanceToTarget=2;angle=96;break;
		//Cases 148-151 distance=4cm, angle=96, size varies, lengths: x=+15,y=-139
		case 148: target_x=xP+15-10;target_y=yP-139-11;targetSize=0;distanceToTarget=4;angle=96;break;
		case 149: target_x=xP+15-18;target_y=yP-139-11;targetSize=1;distanceToTarget=4;angle=96;break;
		case 150: target_x=xP+15-37;target_y=yP-139-11;targetSize=2;distanceToTarget=4;angle=96;break;
		case 151: target_x=xP+15-90;target_y=yP-139-11;targetSize=3;distanceToTarget=4;angle=96;break;
		//Cases 152-155 distance=8cm, angle=96, size varies, lengths: x=+29,y=-278
		case 152: target_x=xP+29-10;target_y=yP-278-11;targetSize=0;distanceToTarget=8;angle=96;break;
		case 153: target_x=xP+29-18;target_y=yP-278-11;targetSize=1;distanceToTarget=8;angle=96;break;
		case 154: target_x=xP+29-37;target_y=yP-278-11;targetSize=2;distanceToTarget=8;angle=96;break;
		case 155: target_x=xP+29-90;target_y=yP-278-11;targetSize=3;distanceToTarget=8;angle=96;break;
		//Cases 156-159 distance=16cm, angle=96, size varies, lengths: x=+59,y=-557
		case 156: target_x=xP+59-10;target_y=yP-557-11;targetSize=0;distanceToTarget=16;angle=96;break;
		case 157: target_x=xP+59-18;target_y=yP-557-11;targetSize=1;distanceToTarget=16;angle=96;break;
		case 158: target_x=xP+59-37;target_y=yP-557-11;targetSize=2;distanceToTarget=16;angle=96;break;
		case 159: target_x=xP+59-90;target_y=yP-557-11;targetSize=3;distanceToTarget=16;angle=96;break;

		//Cases 160-163 distance=1cm, angle=108, size varies, lengths: x=+11,y=-33
		case 160: target_x=xP+11-10;target_y=yP-33-11;targetSize=0;distanceToTarget=1;angle=108;break;
		case 161: target_x=xP+11-18;target_y=yP-33-11;targetSize=1;distanceToTarget=1;angle=108;break;
		case 162: target_x=xP+11-37;target_y=yP-33-11;targetSize=2;distanceToTarget=1;angle=108;break;
		case 163: target_x=xP+11-90;target_y=yP-33-11;targetSize=3;distanceToTarget=1;angle=108;break;
		//Cases 164-167 distance=2cm, angle=108, size varies, lengths: x=+22,y=-67
		case 164: target_x=xP+22-10;target_y=yP-67-11;targetSize=0;distanceToTarget=2;angle=108;break;
		case 165: target_x=xP+22-18;target_y=yP-67-11;targetSize=1;distanceToTarget=2;angle=108;break;
		case 166: target_x=xP+22-37;target_y=yP-67-11;targetSize=2;distanceToTarget=2;angle=108;break;
		case 167: target_x=xP+22-90;target_y=yP-67-11;targetSize=3;distanceToTarget=2;angle=108;break;
		//Cases 168-171 distance=4cm, angle=108, size varies, lengths: x=+43,y=-133
		case 168: target_x=xP+43-10;target_y=yP-133-11;targetSize=0;distanceToTarget=4;angle=108;break;
		case 169: target_x=xP+43-18;target_y=yP-133-11;targetSize=1;distanceToTarget=4;angle=108;break;
		case 170: target_x=xP+43-37;target_y=yP-133-11;targetSize=2;distanceToTarget=4;angle=108;break;
		case 171: target_x=xP+43-90;target_y=yP-133-11;targetSize=3;distanceToTarget=4;angle=108;break;
		//Cases 172-175 distance=8cm, angle=108, size varies, lengths: x=+87,y=-266
		case 172: target_x=xP+87-10;target_y=yP-266-11;targetSize=0;distanceToTarget=8;angle=108;break;
		case 173: target_x=xP+87-18;target_y=yP-266-11;targetSize=1;distanceToTarget=8;angle=108;break;
		case 174: target_x=xP+87-37;target_y=yP-266-11;targetSize=2;distanceToTarget=8;angle=108;break;
		case 175: target_x=xP+87-90;target_y=yP-266-11;targetSize=3;distanceToTarget=8;angle=108;break;
		//Cases 176-179 distance=16cm, angle=108, size varies, lengths: x=+173,y=-533
		case 176: target_x=xP+173-10;target_y=yP-533-11;targetSize=0;distanceToTarget=16;angle=108;break;
		case 177: target_x=xP+173-18;target_y=yP-533-11;targetSize=1;distanceToTarget=16;angle=108;break;
		case 178: target_x=xP+173-37;target_y=yP-533-11;targetSize=2;distanceToTarget=16;angle=108;break;
		case 179: target_x=xP+173-90;target_y=yP-533-11;targetSize=3;distanceToTarget=16;angle=108;break;

		//Cases 180-183 distance=1cm, angle=120, size varies, lengths: x=+18,y=-30
		case 180: target_x=xP+11-10;target_y=yP-33-11;targetSize=0;distanceToTarget=1;angle=120;break;
		case 181: target_x=xP+11-18;target_y=yP-33-11;targetSize=1;distanceToTarget=1;angle=120;break;
		case 182: target_x=xP+11-37;target_y=yP-33-11;targetSize=2;distanceToTarget=1;angle=120;break;
		case 183: target_x=xP+11-90;target_y=yP-33-11;targetSize=3;distanceToTarget=1;angle=120;break;
		//Cases 184-187 distance=2cm, angle=120, size varies, lengths: x=+35,y=-61
		case 184: target_x=xP+22-10;target_y=yP-67-11;targetSize=0;distanceToTarget=2;angle=120;break;
		case 185: target_x=xP+22-18;target_y=yP-67-11;targetSize=1;distanceToTarget=2;angle=120;break;
		case 186: target_x=xP+22-37;target_y=yP-67-11;targetSize=2;distanceToTarget=2;angle=120;break;
		case 187: target_x=xP+22-90;target_y=yP-67-11;targetSize=3;distanceToTarget=2;angle=120;break;
		//Cases 188-191 distance=4cm, angle=120, size varies, lengths: x=+70,y=-121
		case 188: target_x=xP+43-10;target_y=yP-133-11;targetSize=0;distanceToTarget=4;angle=120;break;
		case 189: target_x=xP+43-18;target_y=yP-133-11;targetSize=1;distanceToTarget=4;angle=120;break;
		case 190: target_x=xP+43-37;target_y=yP-133-11;targetSize=2;distanceToTarget=4;angle=120;break;
		case 191: target_x=xP+43-90;target_y=yP-133-11;targetSize=3;distanceToTarget=4;angle=120;break;
		//Cases 192-195 distance=8cm, angle=120, size varies, lengths: x=+140,y=-242
		case 192: target_x=xP+87-10;target_y=yP-266-11;targetSize=0;distanceToTarget=8;angle=120;break;
		case 193: target_x=xP+87-18;target_y=yP-266-11;targetSize=1;distanceToTarget=8;angle=120;break;
		case 194: target_x=xP+87-37;target_y=yP-266-11;targetSize=2;distanceToTarget=8;angle=120;break;
		case 195: target_x=xP+87-90;target_y=yP-266-11;targetSize=3;distanceToTarget=8;angle=120;break;
		//Cases 196-199 distance=16cm, angle=120, size varies, lengths: x=+280,y=-485
		case 196: target_x=xP+173-10;target_y=yP-533-11;targetSize=0;distanceToTarget=16;angle=120;break;
		case 197: target_x=xP+173-18;target_y=yP-533-11;targetSize=1;distanceToTarget=16;angle=120;break;
		case 198: target_x=xP+173-37;target_y=yP-533-11;targetSize=2;distanceToTarget=16;angle=120;break;
		case 199: target_x=xP+173-90;target_y=yP-533-11;targetSize=3;distanceToTarget=16;angle=120;break;
	}//end of switch(i)
  }//end of SetNewPositionAndSize(int i)

  public void mouseClicked(MouseEvent mouseEvent) {}

  public void mouseReleased(MouseEvent mouseEvent) {}

  public void mouseEntered(MouseEvent mouseEvent) {}

  public void mouseExited(MouseEvent mouseEvent) {}

  public void mouseDragged(MouseEvent mouseEvent) {}

  public void keyPressed(KeyEvent keyEvent) {}

  public void keyReleased(KeyEvent keyEvent) {}

  class CardJPanel
      extends JPanel {

    public void paintComponent(Graphics g) {
      super.paintComponent(g);
      paintScreen(g);
    }

    public void paintScreen(Graphics g) {
      setBackground(Color.BLACK);
      
      //Draw rectangles according to target size
      g.setColor(TARGET_COLOR);
      if(targetSize==0)
              g.fillRect(target_x, target_y, 21, 22);
      else if(targetSize==1)
              g.fillRect(target_x, target_y, 38, 22);
      else if(targetSize==2)
              g.fillRect(target_x, target_y, 74, 22);
      else if(targetSize==3)
              g.fillRect(target_x, target_y, 180, 22);
              
      //Draw words according to target size
      g.setColor(Color.BLACK);
      g.setFont(new Font("Courier", Font.PLAIN, 30));
      if(targetSize==0)
           g.drawString("A", target_x+1, target_y+20);
      else if(targetSize==1)
           g.drawString("No", target_x+1, target_y+20);
      else if(targetSize==2)
           g.drawString("Card", target_x+1, target_y+20);
      else if(targetSize==3)
           g.drawString("Experiment", target_x+1, target_y+20);
      
      //what is this for???  
      //g.setColor(Color.WHITE);
      //g.drawRect(0, 0, CARD_WIDTH, CARD_HEIGHT);

      //Draw positioning box of size 20x20 on the bottom of the screen
      g.setColor(HOME_COLOR);
      g.fillRect(CARD_CENTER - 10, CARD_HEIGHT - 20, 20, 20);
      
    }//end of paintScreen()
    
  }//end of CardJPanel class
  
    public void ResultsDialog(double aPT, double aE){
                d.setSize(300,150);
                d.setLayout(new FlowLayout(FlowLayout.CENTER));
                d.add(label1);
                d.add(label2);
		timeLabel=new Label(Double.toString(aPT));
                d.add(timeLabel);
                d.add(label3);
                errorLabel=new Label(Double.toString(aE));
		d.add(errorLabel);
                d.addWindowListener(cF);
                d.setVisible(true);
        }//end of ResultsDialog()

        public void actionPerformed(ActionEvent e){
                if(e.getSource()==GetUserAndDeviceInfoButton)
                {
                        System.out.println(deviceName.getText());
                        System.out.println(subjectName.getText());
                        s.dispose();
    			System.out.println("HomingTime,Angle,DistanceToTarget,TargetSize,PositioningTime,NumberOfErrors");
                }
        }//end of actionPerformed()
}
  
